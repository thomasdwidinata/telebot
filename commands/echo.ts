import TelegramBot from 'node-telegram-bot-api'
import { ErrorResponse, SuccessResponse } from '../services/responder'

export const name: string = 'echo'
export const description: string | null = 'Live test'

let bot: TelegramBot

export function injectBot(telegramBot: TelegramBot) {
    bot = telegramBot
    bot.onText(/^\/echo/, action)
}

function action(msg: TelegramBot.Message, match: RegExpExecArray | null) {
    const chatId = msg.chat.id
    try {
        const resp = match && 'input' in match ? match.input.split(/^\/echo/) : 'Huh?' // the captured "whatever"
        return SuccessResponse(bot, msg, resp[resp.length-1])
    } catch (e) {
        return ErrorResponse(bot, msg, e)
    }
}