import TelegramBot from 'node-telegram-bot-api'
import { ErrorResponse, SuccessResponse } from '../services/responder'

export const name: string = 'photo'
export const description: string | null = 'Sample code for handling with single Photos'

let bot: TelegramBot

export function injectBot(telegramBot: TelegramBot) {
    bot = telegramBot
    bot.on('photo', action)
}

async function action(msg: TelegramBot.Message, meta: TelegramBot.Metadata | null) {
    const chatId: number = msg.chat.id
    try {
        const chatId = msg.chat.id

        if (!msg.photo) return bot.sendMessage(chatId, 'The image that you are sending is not parseable. Please resent your image.')
        const imgId = msg.photo
        const imgStream = bot.getFileStream(msg.photo[msg.photo.length-1].file_id)
        const imgUrl = await bot.getFileLink(msg.photo[msg.photo.length-1].file_id)
        const imgCaption = msg.caption
        
        SuccessResponse(bot, msg, `Received image with path: ${imgUrl}`)
    } catch (e) {
        ErrorResponse(bot, msg, e)
    }
}