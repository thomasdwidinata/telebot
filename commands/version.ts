import TelegramBot from 'node-telegram-bot-api'
import Global from '../global'
import { ErrorResponse, SuccessResponse } from '../services/responder'

export const name: string = 'version'
export const description: string | null = 'Check the Bot Version'

let bot: TelegramBot

export function injectBot(telegramBot: TelegramBot) {
    bot = telegramBot
    bot.onText(/^\/version/, action)
}

function action(msg: TelegramBot.Message, match: RegExpExecArray | null) {
    const chatId: number = msg.chat.id
    try {
        const returnMessage: string = `-- ${Global.app.app_name} --\nVersion: ${Global.app.version}\nBuild: ${Global.app.git_hash}`
        SuccessResponse(bot, msg, returnMessage)
    } catch (e) {
        ErrorResponse(bot, msg, e)
    }
}