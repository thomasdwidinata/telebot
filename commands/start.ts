import TelegramBot from 'node-telegram-bot-api'
import { ErrorResponse, SuccessResponse } from '../services/responder'

export const name: string = 'start'
export const description: string | null = 'Starts the user session'

let bot: TelegramBot

export function injectBot(telegramBot: TelegramBot) {
    bot = telegramBot
    bot.onText(/^\/start/, action)
}

function action(msg: TelegramBot.Message, match: RegExpExecArray | null) {
    const chatId: number = msg.chat.id
    try {
        const returnMessage: string = `Welcome to matchapigeonbot! This is just a beginning of choochoomaru's Telegram bot. Feel free to explore this bot.`
        SuccessResponse(bot, msg, returnMessage)
    } catch (e) {
        ErrorResponse(bot, msg, e)
    }
}