import gitRepoInfo from 'git-repo-info'

const git_hash = gitRepoInfo()?.sha.substring(0, 10) || null

const Global = {
    app: {
        app_id: 'telebot',
        app_name: 'Telebot',
        version: '0.1 Alpha',
        git_hash
    }
}

export = Object.freeze(Global)
