import TelegramBot from "node-telegram-bot-api"
import Config from "../config"
import { log } from "./log"

// Responder is independent from global bot instances, instead uses bot given by the caller to make it universal (Just in case someone ran multiple instances of Bots, which you should make it microservices tho...)

export function SuccessResponse(bot: TelegramBot, msg: TelegramBot.Message, message: string | any) {
    const chatId = msg.chat.id
    switch(typeof(message)) {
        case 'string': break
        case 'object':
            message = JSON.stringify(message)
            break
        default:
            message = JSON.stringify(message, Object.getOwnPropertyNames(message)) || '???'
            break
    }
    log(`User ${msg.from?.username} requested: ${msg.text || JSON.stringify(msg)}`)
    return bot.sendMessage(chatId, message)
}

export function ErrorResponse(bot: TelegramBot, msg: TelegramBot.Message, err: string | any | null) {
    const chatId = msg.chat.id
    let errorMessage = `[ ❌ ] 🤖: Whoops, something error occured. Please take a screenshot of this chat and report it to the Administrator. `
    switch(typeof(err)) {
        case 'string':
            errorMessage += `Error: ${err}`
            break
        case 'object':
            errorMessage += `Dump: ${JSON.stringify(err, Object.getOwnPropertyNames(err))}`
            break
        default:
            errorMessage += `Unknown Error: ${err}`
            break
    }
    log(`An error has occured while processing user ${msg.chat.username}'s request: ${JSON.stringify(err)}`)
    if (Config.options.debug) bot.sendMessage(chatId, errorMessage)
    log(err)
}

