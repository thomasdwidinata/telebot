import TelegramBot from 'node-telegram-bot-api'
import bot from './telegramBot'
import { SuccessResponse, ErrorResponse } from './responder'
import { log } from './log'

export function invalidCommand(msg: TelegramBot.Message) {
  try {
    const message = `Invalid command. Please use '/help' command to check for available commands.`
    SuccessResponse(bot, msg, message)
  } catch (e) {
    ErrorResponse(bot, msg, e)
  }
}

export function help(msg: TelegramBot.Message) {
  try {
    const helpMessage = `How to use this bot?
    no idea..
    `
    SuccessResponse(bot, msg, helpMessage)
  } catch (e) {
    ErrorResponse(bot, msg, e)
  }
}

export function ping(msg: TelegramBot.Message) {
  try {
    SuccessResponse(bot, msg, 'pong')
  } catch (e) {
    ErrorResponse(bot, msg, e)
  }
}

export function logMessage(msg: TelegramBot.Message) {
  const groupName = `${msg.chat.title}`
  const user = `${msg.from?.first_name} ${msg.from?.last_name} (${msg.from?.username} - ${msg.from?.id})`
  log(`[ ${groupName}:${user} ] ${msg.text}`)
}