import Redis from 'ioredis'

import Config from '../config'

export const ServiceTitle = 'Redis Service'
const TTL = Config.redis.ttl // In seconds

export const RedisInstance = new Redis({
    host: Config.redis.host,
    port: Config.redis.port,
    password: Config.redis.password,
    retryStrategy: function(times) {
        let delay = Math.min(times*50, 2000)
        return delay
    }
})

export async function GetRedis(key: string): Promise<any> {
    return RedisInstance.get(key)
}

export async function SetRedis(key: string, value: any, ttl: number = TTL): Promise<any> { // Setting to less than 1 means no TTL
    let Response: any
    if (ttl < 1) Response = await RedisInstance.set(key, value) 
    else {
        Response = await RedisInstance.set(key, value)
        await RedisInstance.expire(key, ttl)
    }
    return Response
}

export async function RemoveRedis(key: string): Promise<boolean> {
    let Response: boolean = false

    if (await RedisInstance.del(key)) Response = true

    return Response
}

export async function VerifyRedis(): Promise<boolean> {
    console.info('Testing Redis connection')
    const testString: string = 'It\'s working!'
    await SetRedis('test', testString)
    const response = await GetRedis('test')
    if (response === testString) {
        console.info('Redis connection verified')
        return true
    }
    else {
        console.warn('Unable to connect to Redis')
        return false
    }
}
