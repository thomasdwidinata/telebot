import { readdirSync } from 'fs'
import TelegramBot from 'node-telegram-bot-api'
import { log } from './log'

interface TelegramCommand {
    readonly name: string
    injectBot(bot: TelegramBot): void
    readonly description?: string
}
function isTelegramCommand(object: any): object is TelegramCommand {
    return 'name' in object && 'injectBot' in object
}

export function initCommands(bot: TelegramBot): string[] {
    const commandDir = `${__dirname}/../commands`
    const commands = traverseCommands(commandDir)
    registerCommands(bot, commands)
    return commands.map( (x: TelegramCommand) => x.name )
}

function registerCommands(bot: TelegramBot, commands: TelegramCommand[] = []) {
    for (let command of commands) command.injectBot(bot)
}

function traverseCommands(dir: string) {
    let commands: TelegramCommand[] = []
    readdirSync(dir, { withFileTypes: true })
        .filter( (file) => {
            if (file.isFile()) return file.name.slice(-3) === '.js'
            else return false
        })
        .forEach( (file) => {
            let commandTemp: TelegramCommand = require(`${dir}/${file.name}`)
            if (isTelegramCommand(commandTemp)) {
                commands.push(commandTemp)
                log(`Injecting Command '/${commandTemp.name}'...`, 'info')
            } else {
                log(`Unable to add command '${file.name.split('.').slice(0, -1).join('.')}'. Invalid command structure that it cannot be converted into Interface 'TelegramCommand'. Skipping.`, 'warn')
            }
        })
    return commands
}