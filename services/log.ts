import Config from '../config'
import { LogType, LogVerbosity } from '../protocols/types'
import { unixTime } from './time'

const logType: LogType = Config.options.log_type
const emojiList = {
    'debug': '🔨',
    'log': '📃',
    'info': 'ℹ',
    'warn': '⚠',
    'error': '❌',
}

export function log(data: any, verbosity: LogVerbosity = 'log', emoji: string = '') {
    let stringifiedData: any = data
    try {
        stringifiedData = JSON.stringify(data)
    } catch (e) {}

    const logString = `${emoji ? emoji : emojiList[verbosity]} [ ${unixTime()} ] ${stringifiedData}`
    switch(logType) {
        case 'file':
            console.warn('File coming soon')
        case 'sqlite':
            console.warn('SQLite coming soon')
        default:
        case 'console':
            console[verbosity](logString)
            break
    }
}