import TelegramBot from 'node-telegram-bot-api'
import Config from '../config'

// Replace the value below with the Telegram token you receive from @BotFather
const token: string = Config.telegram.bot_token || ''
if (!token) throw 'No Telegram bot token specified. Save it on .env file and give it a key \'TELEGRAM_BOT_TOKEN\'.'

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, { polling: true, onlyFirstMatch: true })

export = bot