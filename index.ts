import Global from './global'
console.info(`${Global.app.app_name} (${Global.app.app_id})\nVersion: ${Global.app.version}\nBuild: ${Global.app.git_hash}`)
console.info(`⏳ [ ${Date()} ] Starting Telegram Bot...`)

import bot from './services/telegramBot'
import { initCommands } from './services/commander'
import {
  help,
  invalidCommand,
  logMessage,
  ping
} from './services/essentialCommands'
import { log } from './services/log'

initCommands(bot)

// Inject Essentials Commands
bot.on('message', logMessage)
bot.onText(/^\/ping/, ping)
bot.onText(/^\/help/, help)
bot.onText(/^\//, invalidCommand) // Fallback

log(`Telegram Bot has been started. Everyone can start to chat me now!`, 'info', '✅')