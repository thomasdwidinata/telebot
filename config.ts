import dotenv from 'dotenv'
import { LogType } from './protocols/types'
dotenv.config()

const Config = {
    telegram: {
        bot_token: process.env.TELEGRAM_BOT_TOKEN || '',
        bot_name: process.env.TELEGRAM_BOT_NAME || '',
    },
    redis: {
        host: process.env.REDIS_HOST || 'localhost',
        port: process.env.REDIS_PORT ? parseInt(process.env.REDIS_PORT) : 6379,
        password: process.env.REDIS_PASSWORD || '',
        ttl: process.env.REDIS_TTL ? parseInt(process.env.REDIS_TTL) : 300, // In Seconds
    },
    database: {
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT || 3306,
        username: process.env.DB_USERNAME || 'root',
        password: process.env.DB_PASSWORD || '',
        db_name: process.env.DB_NAME || 'telebot'
    },
    options: {
        log_type: (process.env.LOG_TYPE || 'console') as LogType,
        debug: process.env.NODE_ENV === 'debug' ? true : false
    }
}

if (Config.options.debug) console.warn('⚠ Debugging mode is enabled based on NODE_ENV is set to "debug"')

export = Object.freeze(Config)