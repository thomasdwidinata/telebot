export type LogType = 'console' | 'file' | 'sqlite'
export type LogVerbosity = 'debug' | 'log' | 'info' | 'warn' | 'error'